'use strict'

module.exports = (dir, cb) => {
  const cwd = process.cwd()
  try {
    process.chdir(dir)
    cb()
  } finally {
    process.chdir(cwd)
  }
}
