'use strict'

const extractDescription = require('./extract-description')
const fs = require('fs')
const insertDescription = require('./insert-description')
const limitDescription = require('./limit-description')
const lines = require('./lines')
const loadDocument = require('./load-document')

module.exports = (filename, limit) => {
  const doc = loadDocument(filename)
  if (doc.hasAttribute('description')) return
  const [descriptionLines, replacementRange] = extractDescription(doc)
  if (!descriptionLines) return
  const [description, remainder] = limitDescription(descriptionLines.join('\n'), limit)
  const sourceLines = lines(fs.readFileSync(filename, 'utf8'))
  let subs
  if (doc.applySubstitutions(description, ['quotes']) !== description) subs = ~description.indexOf('{') ? 'a,q' : 'q'
  // NOTE: Document#getHeader()#getLineNumber() always returns the line number of the document title
  insertDescription(sourceLines, doc.getHeader().getLineNumber(), replacementRange, description, remainder, subs)
  fs.writeFileSync(filename, sourceLines.join(''), 'utf8')
}
