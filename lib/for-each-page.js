'use strict'

const fs = require('fs')
const path = require('path')

module.exports = (fn) => {
  return fs.readdirSync('modules').forEach((_module) => forEachAsciiDocFile(path.join('modules', _module, 'pages'), fn))
}

function forEachAsciiDocFile (dir, fn) {
  let dirents
  try {
    dirents = fs.readdirSync(dir, { withFileTypes: true })
  } catch {
    return
  }
  dirents.forEach((dirent) => {
    const name = dirent.name
    if (name.startsWith('_')) return
    if (name.endsWith('.adoc')) {
      fn(path.join(dir, name))
    } else if (dirent.isDirectory()) {
      forEachAsciiDocFile(path.join(dir, name), fn)
    }
  })
}
