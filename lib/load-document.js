'use strict'

const Asciidoctor = require('@asciidoctor/core')()

module.exports = (filename) => {
  return Asciidoctor.loadFile(filename, { safe: 'safe', sourcemap: true, logger: Asciidoctor.NullLogger.create() })
}
