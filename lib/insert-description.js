'use strict'

module.exports = (sourceLines, atLine, replacementRange, description, remainder, subs) => {
  if (replacementRange) {
    if (description.startsWith('*Goal*:')) {
      description = description.substr(8)
      sourceLines.splice(replacementRange[0], replacementRange[1], `*Goal*: {description}${remainder}\n`)
    } else {
      sourceLines.splice(replacementRange[0], replacementRange[1], `{description}${remainder}\n`)
    }
  }
  if (subs) description = `pass:${subs}[${description}]`
  sourceLines.splice(atLine, 0, `:description: ${description.replace(/\n/g, ' \\\n')}\n`)
}
