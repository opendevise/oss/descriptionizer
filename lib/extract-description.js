'use strict'

const { loggers } = require('winston')
const logger = loggers.get('descriptionizer')

module.exports = (doc) => {
  if (!(doc.getHeader().parent && doc.getDocumentTitle())) return []
  let block, firstSection, descriptionLines, replacementRange
  if (
    (block = doc.findBy({ style: 'abstract' })[0]) &&
    (!(firstSection = doc.getSections[0]) || block.getLineNumber() < firstSection.getLineNumber())
  ) {
    if ((descriptionLines = getDescriptionLines(block))) {
      replacementRange = [block.getLineNumber() - 1, descriptionLines.length]
    }
  } else if (
    (block = doc.getBlocks()[0]) &&
    (block.getContext() === 'paragraph' ||
      (block.getContext() === 'preamble' && (block = block.getBlocks()[0]).context === 'paragraph'))
  ) {
    if ((descriptionLines = getDescriptionLines(block))) {
      replacementRange = [block.getLineNumber() - 1, descriptionLines.length]
    }
  }
  return [descriptionLines, replacementRange]
}

function getDescriptionLines (block) {
  const lines = block.getSourceLines()
  const firstLine = lines[0]
  if (firstLine === '{description}') return
  if (!firstLine.startsWith('Unresolved directive ')) return lines
  logger.info(
    'cannot extract description from' +
      firstLine.substr(firstLine.indexOf('include::')) +
      ' in ' +
      block.getDocument().getAttribute('docfile')
  )
}
