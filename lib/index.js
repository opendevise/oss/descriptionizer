'use strict'

const chdir = require('./chdir')
const forEachPage = require('./for-each-page')
const fs = require('fs')
const path = require('path')
const populateDescription = require('./populate-description')

module.exports = (filename, limit) => {
  if (filename.endsWith('.adoc')) {
    if (!fs.existsSync(filename)) throw new Error('path does not exist: ' + filename)
    chdir(path.dirname(filename), () => populateDescription(path.basename(filename), limit))
  } else {
    if (!fs.existsSync(path.join(filename, 'antora.yml'))) {
      throw new Error('path is not an Antora content root: ' + filename)
    }
    chdir(filename, () => forEachPage((page) => populateDescription(page, limit)))
  }
}
