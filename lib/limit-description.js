'use strict'

const FULL_STOP_RX = /\.(?= |\n|$)/

module.exports = (description, limit) => {
  let remainder = ''
  if (limit < 1 || description.length <= limit) return [description, remainder]
  let match, splitIdx
  // 1. if there's a full stop within the limit, cut at the last full stop
  // 2. if there's a full stop beyond the limit, cut at the first full stop
  // 3. otherwise, take the whole description (since we don't want to truncate a sentence)
  if (FULL_STOP_RX.test(description.substr(0, limit))) {
    remainder = description.substr(limit)
    description = description.substr(0, limit)
    const re = new RegExp(FULL_STOP_RX, 'g')
    while ((match = re.exec(description))) splitIdx = match.index + 1
    remainder = `${description.substr(splitIdx)}${remainder}`
    description = description.substr(0, splitIdx)
  } else if ((match = description.match(/\.(?= |\n)/))) {
    splitIdx = match.index + 1
    remainder = description.substr(splitIdx)
    description = description.substr(0, splitIdx)
  }
  return [description, remainder]
}
