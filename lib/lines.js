'use strict'

const NEWLINE_RX = /(?<=\r?\n)/

module.exports = (string) => string.split(NEWLINE_RX)
